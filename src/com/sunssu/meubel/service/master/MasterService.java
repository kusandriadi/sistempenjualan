/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.master;

import com.sunssu.meubel.entity.Barang;
import com.sunssu.meubel.entity.Pelanggan;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface MasterService {

    public void saveOrUpdate(Barang barang);
    public void delete(Barang barang);
    public Barang getBarang(String barang);
    public List<Barang> getbBarangs();
    public String autoNumberBarang();

    public void saveOrUpdate(Pelanggan pelanggan);
    public void delete(Pelanggan pelanggan);
    public Pelanggan getPelanggan(String pelanggan);
    public List<Pelanggan> getPelanggans();
    public String autoNumberPelanggan();
}
