/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.master.impl;

import com.sunssu.meubel.dao.master.BarangDao;
import com.sunssu.meubel.dao.master.PelangganDao;
import com.sunssu.meubel.entity.Barang;
import com.sunssu.meubel.entity.Pelanggan;
import com.sunssu.meubel.service.master.MasterService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kus Andriadi
 */
@Repository("masterService")
@Transactional(readOnly=true)
public class MasterServiceImpl implements MasterService{

    private BarangDao barangDao;

    private PelangganDao pelangganDao;

    @Autowired
    public void setBarangDao(@Qualifier("barangDaoHibernate")
            BarangDao barangDao) {
        this.barangDao = barangDao;
    }

    @Autowired
    public void setPelangganDao(@Qualifier("pelangganDaoHibernate")
            PelangganDao pelangganDao) {
        this.pelangganDao = pelangganDao;
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Barang barang) {
        barangDao.saveOrUpdate(barang);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Barang barang) {
        barangDao.delete(barang);
    }

    public Barang getBarang(String barang) {
        return barangDao.getBarang(barang);
    }

    public List<Barang> getbBarangs() {
        return barangDao.getbBarangs();
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Pelanggan pelanggan) {
        pelangganDao.saveOrUpdate(pelanggan);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Pelanggan pelanggan) {
        pelangganDao.delete(pelanggan);
    }

    public Pelanggan getPelanggan(String pelanggan) {
        return pelangganDao.getPelanggan(pelanggan);
    }

    public List<Pelanggan> getPelanggans() {
        return pelangganDao.getPelanggans();
    }

    public String autoNumberBarang() {
        return barangDao.autoNumberBarang();
    }

    public String autoNumberPelanggan() {
        return pelangganDao.autoNumberPelanggan();
    }

}
