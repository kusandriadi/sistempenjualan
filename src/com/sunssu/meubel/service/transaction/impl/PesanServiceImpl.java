/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.transaction.impl;

import com.sunssu.meubel.dao.transaction.FakturDao;
import com.sunssu.meubel.dao.transaction.PesanDao;
import com.sunssu.meubel.dao.transaction.PesananDao;
import com.sunssu.meubel.entity.Faktur;
import com.sunssu.meubel.entity.Pesan;
import com.sunssu.meubel.entity.Pesanan;
import com.sunssu.meubel.service.transaction.PesanService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kus Andriadi
 */
@Repository("pesanService")
@Transactional(readOnly=true)
public class PesanServiceImpl implements PesanService{
    
    private FakturDao fakturDao;

    private PesanDao pesanDao;

    private PesananDao pesananDao;

    @Autowired
    public void setFakturDao(@Qualifier("fakturDaoHibernate")
            FakturDao fakturDao) {
        this.fakturDao = fakturDao;
    }

    @Autowired
    public void setPesanDao(@Qualifier("pesanDaoHibernate")
            PesanDao pesanDao) {
        this.pesanDao = pesanDao;
    }

    @Autowired
    public void setPesananDao(@Qualifier("pesananDaoHibernate")
            PesananDao pesananDao) {
        this.pesananDao = pesananDao;
    }
    
    //dao Faktur
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Faktur faktur) {
        fakturDao.saveOrUpdate(faktur);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Faktur faktur) {
        fakturDao.delete(faktur);
    }

    public Faktur getFaktur(String faktur) {
        return fakturDao.getFaktur(faktur);
    }

    public List<Faktur> getFaktur() {
        return fakturDao.getFaktur();
    }

    public String autoNumberFaktur() {
        return fakturDao.autoNumberFaktur();
    }


    //dao Pesan
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Pesan pesan) {
        pesanDao.saveOrUpdate(pesan);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Pesan pesan) {
        pesanDao.delete(pesan);
    }

    public Pesan getPesanByNoSP(String pesan) {
        return pesanDao.getPesanByNoSP(pesan);
    }

    public Pesan getPesanByKdBarang(String pesan) {
        return pesanDao.getPesanByKdBarang(pesan);
    }

    public List<Pesan> getPesan() {
        return pesanDao.getPesan();
    }

    public List<String> getKodeBarangByNoSp(int pesan) {
        return pesanDao.getKodeBarangByNoSp(pesan);
    }

    public Pesan getDataBarangForSuratJalan(String barang, int pesan) {
       return pesanDao.getDataBarangForSuratJalan(barang, pesan);
    }


    //dao Pesanan
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Pesanan pesanan) {
        pesananDao.saveOrUpdate(pesanan);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Pesanan pesanan) {
        pesananDao.delete(pesanan);
    }

    public Pesanan getPesanan(int pesanan) {
        return pesananDao.getPesanan(pesanan);
    }

    @Transactional
    public List<Pesanan> getPesanan() {
        return pesananDao.getPesanan();
    }

    public String autoNumberPesanan() {
        return pesananDao.autoNumberPesanan();
    }

    public Pesan getForSuratJalan(int pesan, String pesanBarang) {
        return pesanDao.getForSuratJalan(pesan, pesanBarang);
    }
}
