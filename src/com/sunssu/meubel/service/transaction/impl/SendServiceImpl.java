/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.transaction.impl;

import com.sunssu.meubel.dao.otherTransaction.KembaliDao;
import com.sunssu.meubel.dao.otherTransaction.KirimDao;
import com.sunssu.meubel.dao.otherTransaction.ReturDao;
import com.sunssu.meubel.dao.otherTransaction.SuratJalanDao;
import com.sunssu.meubel.entity.Kembali;
import com.sunssu.meubel.entity.Kirim;
import com.sunssu.meubel.entity.Retur;
import com.sunssu.meubel.entity.Sj;
import com.sunssu.meubel.service.transaction.SendService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kus Andriadi
 */
@Repository("sendService")
@Transactional(readOnly=true)
public class SendServiceImpl implements SendService{

    private KembaliDao kembaliDao;

    private KirimDao kirimDao;

    private ReturDao returDao;

    private SuratJalanDao suratJalanDao;

    @Autowired
    public void setKembaliDao(@Qualifier("kembaliDaoHibernate")
            KembaliDao kembaliDao) {
        this.kembaliDao = kembaliDao;
    }

    @Autowired
    public void setKirimDao(@Qualifier("kirimDaoHibernate")
            KirimDao kirimDao) {
        this.kirimDao = kirimDao;
    }

    @Autowired
    public void setReturDao(@Qualifier("returDaoHibernate")
            ReturDao returDao) {
        this.returDao = returDao;
    }

    @Autowired
    public void setSuratJalanDao(@Qualifier("suratJalanDaoHibernate")
            SuratJalanDao suratJalanDao) {
        this.suratJalanDao = suratJalanDao;
    }

    //dao kembali
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Kembali kembali) {
        kembaliDao.saveOrUpdate(kembali);
    }

    @Transactional(readOnly=false,propagation=Propagation.REQUIRED)
    public void delete(Kembali kembali) {
        kembaliDao.delete(kembali);
    }

    public Kembali getKembaliByNoRetur(String kembali) {
        return kembaliDao.getKembaliByNoRetur(kembali);
    }

    public Kembali getKembaliByKdBarang(String kembali) {
        return kembaliDao.getKembaliByKdBarang(kembali);
    }

    public List<Kembali> getKembali() {
        return kembaliDao.getKembali();
    }

    //dao kirim
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Kirim kirim) {
        kirimDao.saveOrUpdate(kirim);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Kirim kirim) {
        kirimDao.delete(kirim);
    }

    public Kirim getKirimByNoSJ(String kirim) {
        return kirimDao.getKirimByKdBarang(kirim);
    }

    public Kirim getKirimByKdBarang(String kirim) {
        return kirimDao.getKirimByNoSJ(kirim);
    }

    public List<Kirim> getKirim() {
        return kirimDao.getKirim();
    }

    //dao surat jalan
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Sj suratJalan) {
        suratJalanDao.saveOrUpdate(suratJalan);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Sj suratJalan) {
       suratJalanDao.delete(suratJalan);
    }

    public Sj getSuratJalan(String sj) {
       return suratJalanDao.getSuratJalan(sj);
    }

    public List<Sj> getSuratJalans() {
        return suratJalanDao.getSuratJalans();
    }

    public String autoNumberSuratJalan() {
       return suratJalanDao.autoNumberSuratJalan();
    }

    //dao retur
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void saveOrUpdate(Retur retur) {
        returDao.saveOrUpdate(retur);
    }

    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public void delete(Retur retur) {
        returDao.delete(retur);
    }

    public Retur getRetur(String retur) {
        return returDao.getRetur(retur);
    }

    public List<Retur> getReturs() {
        return returDao.getReturs();
    }

    public String autoNumberRetur() {
       return returDao.autoNumberRetur();
    }

}
