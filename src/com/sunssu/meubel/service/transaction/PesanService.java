/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.transaction;

import com.sunssu.meubel.entity.Faktur;
import com.sunssu.meubel.entity.Pesan;
import com.sunssu.meubel.entity.Pesanan;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface PesanService {

    public void saveOrUpdate(Faktur faktur);
    public void delete(Faktur faktur);
    public Faktur getFaktur(String faktur);
    public List<Faktur> getFaktur();
    public String autoNumberFaktur();


    public void saveOrUpdate(Pesan pesan);
    public void delete(Pesan pesan);
    public Pesan getPesanByNoSP(String pesan);
    public Pesan getPesanByKdBarang(String pesan);
    public Pesan getForSuratJalan(int pesan, String pesanBarang);
    public List<Pesan> getPesan();
    public List<String> getKodeBarangByNoSp(int pesan);
    public Pesan getDataBarangForSuratJalan(String barang, int pesan);


    public void saveOrUpdate(Pesanan pesanan);
    public void delete(Pesanan pesanan);
    public Pesanan getPesanan(int pesanan);
    public List<Pesanan> getPesanan();
    public String autoNumberPesanan();

}
