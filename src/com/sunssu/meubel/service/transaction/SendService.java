/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.service.transaction;

import com.sunssu.meubel.entity.Kembali;
import com.sunssu.meubel.entity.Kirim;
import com.sunssu.meubel.entity.Retur;
import com.sunssu.meubel.entity.Sj;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface SendService {

    public void saveOrUpdate(Kembali kembali);
    public void delete(Kembali kembali);
    public Kembali getKembaliByNoRetur(String kembali);
    public Kembali getKembaliByKdBarang(String kembali);
    public List<Kembali> getKembali();


    public void saveOrUpdate(Kirim kirim);
    public void delete(Kirim kirim);
    public Kirim getKirimByNoSJ(String kirim);
    public Kirim getKirimByKdBarang(String kirim);
    public List<Kirim> getKirim();

    public void saveOrUpdate(Sj sj);
    public void delete(Sj sj);
    public Sj getSuratJalan(String sj);
    public List<Sj> getSuratJalans();
    public String autoNumberSuratJalan();

    public void saveOrUpdate(Retur retur);
    public void delete(Retur retur);
    public Retur getRetur(String retur);
    public List<Retur> getReturs();
    public String autoNumberRetur();
}
