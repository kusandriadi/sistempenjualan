/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ReturPanel.java
 *
 * Created on Jun 22, 2010, 2:48:45 AM
 */

package com.sunssu.meubel.UI.transaction;

import com.sunssu.meubel.HibernateTemplate.HBTemplate;
import com.sunssu.meubel.entity.Barang;
import com.sunssu.meubel.entity.Faktur;
import com.sunssu.meubel.entity.Kembali;
import com.sunssu.meubel.entity.KembaliPK;
import com.sunssu.meubel.entity.Pelanggan;
import com.sunssu.meubel.entity.Pesan;
import com.sunssu.meubel.entity.Pesanan;
import com.sunssu.meubel.entity.Retur;
import com.sunssu.meubel.entity.Sj;
import com.sunssu.meubel.table.model.ReturTableModel;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

/**
 *
 * @author Kus Andriadi
 */
public class ReturPanel extends javax.swing.JPanel {

    /** Creates new form ReturPanel */
    public ReturPanel() {
        initComponents();
        list = new ArrayList<Kembali>();
        returTableModel = new ReturTableModel(list);
        tableRetur.setModel(returTableModel);

        TanggalSJDate.setCalendar(Calendar.getInstance());
        TanggalPesanDate.setCalendar(Calendar.getInstance());
        TanggalSJDate.setDateFormatString("dd-MM-yyyy");
        TanggalPesanDate.setDateFormatString("dd-MM-yyyy");
        autoNumber();
    }

    private void clearAll(){
        txtKodeSJ.setText("");
        TanggalSJDate.setCalendar(Calendar.getInstance());
        TanggalPesanDate.setCalendar(Calendar.getInstance());
        txtKodePesan.setText("");
        txtKodePelanggan.setText("");
        txtNamaPelanggan.setText("");

        btnSimpan.setEnabled(false);
        cmbKodeBarang.removeAllItems();
        cmbKodeBarang.updateUI();

        clearBarang();
        clearTable();
        autoNumber();
    }

    private void clearBarang(){
        txtHarga.setText("0");
        txtJumlah.setText("0");
        txtJumlahRetur.setText("0");
        txtKeterangan.setText("");
        txtNamaBarang.setText("");
    }

    private void clearTable(){
        TableModel model = tableRetur.getModel();
        int numrows = model.getRowCount();
        for(int i=numrows-1;i>=0;i--){
            returTableModel.deleteTableRetur(i);
        }
    }

    private void justNumber(KeyEvent evt){
        char kar = evt.getKeyChar();
	if(!(Character.isDigit(kar)) || (kar==KeyEvent.VK_BACK_SPACE)){
		getToolkit().beep();
		evt.consume();
	}
    }

    private void autoNumber() {
        auto = HBTemplate.getSendService().autoNumberRetur();
	if (auto.equals("")) {
            txtKodeRetur.setText("R0001");
	} else {
            auto = auto.substring(1);
	int angka = Integer.parseInt(auto);
	angka = angka + 1;
	if (angka <= 9) {
		auto = "000" + angka;
	} else if (angka >= 10 && angka <= 99) {
		auto = "00" + angka;
        } else if (angka >= 100 && angka <= 999) {
		auto = "0" + angka;
	} else if (angka >= 1000 && angka <= 9999) {
		auto = "" + angka;
	}
	if (auto.length() > 5) {
		auto = auto.substring(auto.length() - 5, auto.length());
	}
	auto = "R" + auto;
	txtKodeRetur.setText(auto);
	}
    }

    private void cariSJ(){
        if(txtKodeSJ.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Anda belum menginput Surat Jalan!");
        }else{
            cmbKodeBarang.removeAllItems();
            cmbKodeBarang.updateUI();
            try {
                try {
                    suratJalan = HBTemplate.getSendService().getSuratJalan(txtKodeSJ.getText().toUpperCase());
                    TanggalSJDate.setDate(suratJalan.getTglSj());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Surat Jalan yang anda cari salah!");
                }

                faktur = HBTemplate.getPesanService().getFaktur(suratJalan.getNoFak().getNoFak());
                txtKodePesan.setText(String.valueOf(faktur.getNoSp().getNoSp()));

                pesanan = HBTemplate.getPesanService().getPesanan(Integer.valueOf(txtKodePesan.getText()));
                txtKodePelanggan.setText(pesanan.getKdPlg().getKdPlg());

                pelanggan = HBTemplate.getMasterService().getPelanggan(txtKodePelanggan.getText());
                txtNamaPelanggan.setText(pelanggan.getNmPlg());

                listPesan = HBTemplate.getPesanService().getKodeBarangByNoSp(Integer.valueOf(txtKodePesan.getText()));
                
                for(int i = 0;i<=listPesan.size()-1;i++){
                   cmbKodeBarang.addItem(listPesan.get(i).toString());
                }
            } catch (Exception e) {
            }
        }
    }

    private void cariDataBarang(){
                try{
                    pesan = null;
                     pesan = HBTemplate.getPesanService().
                             getDataBarangForSuratJalan(cmbKodeBarang.getSelectedItem().toString(),
                             Integer.valueOf(txtKodePesan.getText()));
                   
                     txtNamaBarang.setText(pesan.getBarang().getNmBrng());
                     txtHarga.setText(String.valueOf(pesan.getBarang().getHarSat()));
                     txtJumlah.setText(String.valueOf(pesan.getJmlPesan()));

                }catch(Exception e){
                }
    }

    private void addItemAction(){
        try {
            simpanSementara = 0;
            simpanSementara = Integer.valueOf(txtJumlah.getText());
            if(Integer.valueOf(txtJumlahRetur.getText()) > simpanSementara){
                JOptionPane.showMessageDialog(null, "Jumlah Retur yang anda masukan tidak tepat!");
            }else{
                if(checkRetur()){
                    kembali = new Kembali();
                    kembali.setBarang(new Barang(cmbKodeBarang.getSelectedItem().toString().toUpperCase()));
                    kembali.setJmlhBarang(Integer.valueOf(txtJumlahRetur.getText()));
                    kembali.setKeterangan(txtKeterangan.getText());
                    returTableModel.addTableRetur(kembali);
                    
                    simpanSementara = cmbKodeBarang.getSelectedIndex();
                    cmbKodeBarang.removeItemAt(simpanSementara);
                    if(cmbKodeBarang.getItemCount() == 1){
                        cariDataBarang();
                    }
                    clearBarang();
                }else{
                    JOptionPane.showMessageDialog(null, "Detil retur ada yang belum diisi");
                }
            }
            simpanSementara = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkRetur(){
        if(!cmbKodeBarang.getSelectedItem().toString().equals("")||
                !cmbKodeBarang.getSelectedItem().toString().equals("-- Pilih Kode Barang --")||
                !txtJumlahRetur.getText().trim().equals("") ||
                !txtJumlah.getText().trim().equals("0")||
                !txtKeterangan.getText().trim().equals("")){
                return true;
        }else{
                return false;
        }
    }

    private void simpanRetur(){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        date = calendar.getTime();
        if(!txtKodeSJ.getText().trim().equals("")){
            retur = new Retur();
            retur.setNoRetur(txtKodeRetur.getText());
            retur.setNoSj(new Sj(txtKodeSJ.getText()));
            retur.setTglReturn(date);
            HBTemplate.getSendService().saveOrUpdate(retur);
        }
    }

        private void takeAndSaveFromTable(){
        try {
            TableModel model = tableRetur.getModel();
             int numrows = model.getRowCount();
             for(int i = 0;i<numrows;i++){
                kembali = new Kembali();
                kembali.setKembaliPK(new KembaliPK(model.getValueAt(i,0).toString().toUpperCase(), txtKodeRetur.getText().toUpperCase()));
                kembali.setJmlhBarang(Integer.valueOf(model.getValueAt(i, 1).toString()));
                kembali.setKeterangan(txtKeterangan.getText());
                HBTemplate.getSendService().saveOrUpdate(kembali);
                JOptionPane.showMessageDialog(null, "Berhasil Input Retur");
             }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "gagal input retur");
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTable = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableRetur = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        txtKodeRetur = new javax.swing.JTextField();
        panelSuratJalan = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtKodeSJ = new javax.swing.JTextField();
        txtKodePesan = new javax.swing.JTextField();
        TanggalSJDate = new com.toedter.calendar.JDateChooser();
        TanggalPesanDate = new com.toedter.calendar.JDateChooser();
        txtKodePelanggan = new javax.swing.JTextField();
        txtNamaPelanggan = new javax.swing.JTextField();
        btnCariKodeSJ = new javax.swing.JButton();
        panelDetilRetur = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtNamaBarang = new javax.swing.JTextField();
        txtHarga = new javax.swing.JTextField();
        txtJumlah = new javax.swing.JTextField();
        txtJumlahRetur = new javax.swing.JTextField();
        btnAddItem = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtKeterangan = new javax.swing.JTextField();
        cmbKodeBarang = new javax.swing.JComboBox();
        btnSimpan = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        panelTable.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Table Retur", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 0, 0))); // NOI18N

        tableRetur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tableRetur);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Kode Retur :");

        txtKodeRetur.setEnabled(false);

        javax.swing.GroupLayout panelTableLayout = new javax.swing.GroupLayout(panelTable);
        panelTable.setLayout(panelTableLayout);
        panelTableLayout.setHorizontalGroup(
            panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTableLayout.createSequentialGroup()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtKodeRetur, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(433, 433, 433))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)
        );
        panelTableLayout.setVerticalGroup(
            panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTableLayout.createSequentialGroup()
                .addGroup(panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtKodeRetur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelSuratJalan.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detil Surat Jalan", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 0, 0))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Kode SJ :");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Tanggal SJ :");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Kode Pesan :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Tanggal Pesan :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Kode Pelanggan :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Nama Pelanggan :");

        txtKodeSJ.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtKodeSJKeyPressed(evt);
            }
        });

        txtKodePesan.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtKodePesan.setEnabled(false);

        TanggalSJDate.setEnabled(false);
        TanggalSJDate.setFont(new java.awt.Font("Tahoma", 1, 11));

        TanggalPesanDate.setEnabled(false);
        TanggalPesanDate.setFont(new java.awt.Font("Tahoma", 1, 11));

        txtKodePelanggan.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtKodePelanggan.setEnabled(false);

        txtNamaPelanggan.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtNamaPelanggan.setEnabled(false);

        btnCariKodeSJ.setText("Cari");
        btnCariKodeSJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariKodeSJActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelSuratJalanLayout = new javax.swing.GroupLayout(panelSuratJalan);
        panelSuratJalan.setLayout(panelSuratJalanLayout);
        panelSuratJalanLayout.setHorizontalGroup(
            panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSuratJalanLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtKodePesan, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                    .addComponent(TanggalPesanDate, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                    .addComponent(txtKodePelanggan, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                    .addComponent(txtNamaPelanggan, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSuratJalanLayout.createSequentialGroup()
                        .addComponent(txtKodeSJ, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCariKodeSJ, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(TanggalSJDate, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
                .addGap(27, 27, 27))
        );
        panelSuratJalanLayout.setVerticalGroup(
            panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSuratJalanLayout.createSequentialGroup()
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtKodeSJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCariKodeSJ))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(TanggalSJDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtKodePesan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(TanggalPesanDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtKodePelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelSuratJalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtNamaPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        panelDetilRetur.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detil Retur", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 0, 0))); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Kode Barang :");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Nama Barang :");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Harga :");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Jumlah :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Jumlah Retur :");

        txtNamaBarang.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtNamaBarang.setEnabled(false);

        txtHarga.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtHarga.setEnabled(false);

        txtJumlah.setFont(new java.awt.Font("Tahoma", 1, 11));
        txtJumlah.setEnabled(false);

        txtJumlahRetur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtJumlahReturKeyTyped(evt);
            }
        });

        btnAddItem.setText("Add Item");
        btnAddItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItemActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Keterangan :");

        cmbKodeBarang.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbKodeBarangItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelDetilReturLayout = new javax.swing.GroupLayout(panelDetilRetur);
        panelDetilRetur.setLayout(panelDetilReturLayout);
        panelDetilReturLayout.setHorizontalGroup(
            panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDetilReturLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAddItem)
                    .addGroup(panelDetilReturLayout.createSequentialGroup()
                        .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbKodeBarang, 0, 145, Short.MAX_VALUE)
                            .addComponent(txtKeterangan, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(txtHarga, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(txtJumlah, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(txtJumlahRetur, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(txtNamaBarang, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))))
                .addGap(73, 73, 73))
        );
        panelDetilReturLayout.setVerticalGroup(
            panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDetilReturLayout.createSequentialGroup()
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbKodeBarang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtNamaBarang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtHarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtJumlahRetur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDetilReturLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtKeterangan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddItem)
                .addContainerGap(2, Short.MAX_VALUE))
        );

        btnSimpan.setText("Simpan");
        btnSimpan.setEnabled(false);
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(panelSuratJalan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDetilRetur, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(489, Short.MAX_VALUE)
                .addComponent(btnSimpan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelSuratJalan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDetilRetur, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear)
                    .addComponent(btnSimpan))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtJumlahReturKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtJumlahReturKeyTyped
        // TODO add your handling code here:
        justNumber(evt);
    }//GEN-LAST:event_txtJumlahReturKeyTyped

    private void txtKodeSJKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtKodeSJKeyPressed
        // TODO add your handling code here:
        int obj = evt.getKeyCode();
        if(obj == KeyEvent.VK_ENTER){
            cariSJ();
        }
    }//GEN-LAST:event_txtKodeSJKeyPressed

    private void btnCariKodeSJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariKodeSJActionPerformed
        // TODO add your handling code here:
        cariSJ();
    }//GEN-LAST:event_btnCariKodeSJActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        clearAll();
        clearTable();
    }//GEN-LAST:event_btnClearActionPerformed

    private void cmbKodeBarangItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbKodeBarangItemStateChanged
        // TODO add your handling code here:
        cariDataBarang();
        if(cmbKodeBarang.getItemCount() == 1){
            cariDataBarang();
        }
    }//GEN-LAST:event_cmbKodeBarangItemStateChanged

    private void btnAddItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItemActionPerformed
        // TODO add your handling code here:
        addItemAction();
        btnSimpan.setEnabled(true);
    }//GEN-LAST:event_btnAddItemActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        simpanRetur();
        takeAndSaveFromTable();
        clearAll();
    }//GEN-LAST:event_btnSimpanActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser TanggalPesanDate;
    private com.toedter.calendar.JDateChooser TanggalSJDate;
    private javax.swing.JButton btnAddItem;
    private javax.swing.JButton btnCariKodeSJ;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JComboBox cmbKodeBarang;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelDetilRetur;
    private javax.swing.JPanel panelSuratJalan;
    private javax.swing.JPanel panelTable;
    private javax.swing.JTable tableRetur;
    private javax.swing.JTextField txtHarga;
    private javax.swing.JTextField txtJumlah;
    private javax.swing.JTextField txtJumlahRetur;
    private javax.swing.JTextField txtKeterangan;
    private javax.swing.JTextField txtKodePelanggan;
    private javax.swing.JTextField txtKodePesan;
    private javax.swing.JTextField txtKodeRetur;
    private javax.swing.JTextField txtKodeSJ;
    private javax.swing.JTextField txtNamaBarang;
    private javax.swing.JTextField txtNamaPelanggan;
    // End of variables declaration//GEN-END:variables

    private ReturTableModel returTableModel;

    private String auto;

    private ArrayList<Kembali> list;

    private Sj suratJalan;

    private Faktur faktur;

    private Pesanan pesanan;

    private Pelanggan pelanggan;

    private List<String> listPesan;

    private Kembali kembali;

    private Retur retur;

    private Pesan pesan;
    
    private int simpanSementara;
}
