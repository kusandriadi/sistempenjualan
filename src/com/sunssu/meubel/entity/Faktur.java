/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "faktur", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Faktur.findAll", query = "SELECT f FROM Faktur f"),
    @NamedQuery(name = "Faktur.findByNoFak", query = "SELECT f FROM Faktur f WHERE f.noFak = :noFak"),
    @NamedQuery(name = "Faktur.findByTglFak", query = "SELECT f FROM Faktur f WHERE f.tglFak = :tglFak")})
public class Faktur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "no_fak", nullable = false, length = 5)
    private String noFak;
    @Column(name = "tgl_fak")
    @Temporal(TemporalType.DATE)
    private Date tglFak;
    @JoinColumn(name = "no_sp", referencedColumnName = "no_sp", nullable = false)
    @ManyToOne(optional = false)
    private Pesanan noSp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noFak")
    private List<Sj> sjList;

    public Faktur() {
    }

    public Faktur(String noFak) {
        this.noFak = noFak;
    }

    public String getNoFak() {
        return noFak;
    }

    public void setNoFak(String noFak) {
        this.noFak = noFak;
    }

    public Date getTglFak() {
        return tglFak;
    }

    public void setTglFak(Date tglFak) {
        this.tglFak = tglFak;
    }

    public Pesanan getNoSp() {
        return noSp;
    }

    public void setNoSp(Pesanan noSp) {
        this.noSp = noSp;
    }

    public List<Sj> getSjList() {
        return sjList;
    }

    public void setSjList(List<Sj> sjList) {
        this.sjList = sjList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noFak != null ? noFak.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faktur)) {
            return false;
        }
        Faktur other = (Faktur) object;
        if ((this.noFak == null && other.noFak != null) || (this.noFak != null && !this.noFak.equals(other.noFak))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Faktur[noFak=" + noFak + "]";
    }

}
