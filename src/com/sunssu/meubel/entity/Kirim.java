/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "kirim", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Kirim.findAll", query = "SELECT k FROM Kirim k"),
    @NamedQuery(name = "Kirim.findByNoSj", query = "SELECT k FROM Kirim k WHERE k.kirimPK.noSj = :noSj"),
    @NamedQuery(name = "Kirim.findByKdBrng", query = "SELECT k FROM Kirim k WHERE k.kirimPK.kdBrng = :kdBrng"),
    @NamedQuery(name = "Kirim.findByJmlKirim", query = "SELECT k FROM Kirim k WHERE k.jmlKirim = :jmlKirim")})
public class Kirim implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected KirimPK kirimPK;
    @Column(name = "jml_kirim")
    private Integer jmlKirim;
    @JoinColumn(name = "no_sj", referencedColumnName = "no_sj", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sj sj;
    @JoinColumn(name = "kd_brng", referencedColumnName = "kd_brng", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Barang barang;

    public Kirim() {
    }

    public Kirim(KirimPK kirimPK) {
        this.kirimPK = kirimPK;
    }

    public Kirim(String noSj, String kdBrng) {
        this.kirimPK = new KirimPK(noSj, kdBrng);
    }

    public KirimPK getKirimPK() {
        return kirimPK;
    }

    public void setKirimPK(KirimPK kirimPK) {
        this.kirimPK = kirimPK;
    }

    public Integer getJmlKirim() {
        return jmlKirim;
    }

    public void setJmlKirim(Integer jmlKirim) {
        this.jmlKirim = jmlKirim;
    }

    public Sj getSj() {
        return sj;
    }

    public void setSj(Sj sj) {
        this.sj = sj;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kirimPK != null ? kirimPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kirim)) {
            return false;
        }
        Kirim other = (Kirim) object;
        if ((this.kirimPK == null && other.kirimPK != null) || (this.kirimPK != null && !this.kirimPK.equals(other.kirimPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Kirim[kirimPK=" + kirimPK + "]";
    }

}
