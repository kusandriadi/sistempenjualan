/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "pesanan", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Pesanan.findAll", query = "SELECT p FROM Pesanan p"),
    @NamedQuery(name = "Pesanan.findByNoSp", query = "SELECT p FROM Pesanan p left join fetch p.fakturList WHERE p.noSp = :noSp"),
    @NamedQuery(name = "Pesanan.findByTglSp", query = "SELECT p FROM Pesanan p WHERE p.tglSp = :tglSp"),
    @NamedQuery(name = "Pesanan.findByTotal", query = "SELECT p FROM Pesanan p WHERE p.total = :total")})
public class Pesanan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "no_sp", nullable = false)
    private Integer noSp;
    @Column(name = "tgl_sp")
    @Temporal(TemporalType.DATE)
    private Date tglSp;
    @Column(name = "total")
    private Integer total;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pesanan")
    private List<Pesan> pesanList;
    @JoinColumn(name = "kd_plg", referencedColumnName = "kd_plg")
    @ManyToOne
    private Pelanggan kdPlg;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noSp")
    private List<Faktur> fakturList;

    public Pesanan() {
    }

    public Pesanan(Integer noSp) {
        this.noSp = noSp;
    }

    public Integer getNoSp() {
        return noSp;
    }

    public void setNoSp(Integer noSp) {
        this.noSp = noSp;
    }

    public Date getTglSp() {
        return tglSp;
    }

    public void setTglSp(Date tglSp) {
        this.tglSp = tglSp;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Pesan> getPesanList() {
        return pesanList;
    }

    public void setPesanList(List<Pesan> pesanList) {
        this.pesanList = pesanList;
    }

    public Pelanggan getKdPlg() {
        return kdPlg;
    }

    public void setKdPlg(Pelanggan kdPlg) {
        this.kdPlg = kdPlg;
    }

    public List<Faktur> getFakturList() {
        return fakturList;
    }

    public void setFakturList(List<Faktur> fakturList) {
        this.fakturList = fakturList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noSp != null ? noSp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pesanan)) {
            return false;
        }
        Pesanan other = (Pesanan) object;
        if ((this.noSp == null && other.noSp != null) || (this.noSp != null && !this.noSp.equals(other.noSp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Pesanan[noSp=" + noSp + "]";
    }

}
