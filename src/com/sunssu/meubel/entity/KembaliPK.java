/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Kus Andriadi
 */
@Embeddable
public class KembaliPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "kd_brng", nullable = false, length = 5)
    private String kdBrng;
    @Basic(optional = false)
    @Column(name = "no_retur", nullable = false, length = 5)
    private String noRetur;

    public KembaliPK() {
    }

    public KembaliPK(String kdBrng, String noRetur) {
        this.kdBrng = kdBrng;
        this.noRetur = noRetur;
    }

    public String getKdBrng() {
        return kdBrng;
    }

    public void setKdBrng(String kdBrng) {
        this.kdBrng = kdBrng;
    }

    public String getNoRetur() {
        return noRetur;
    }

    public void setNoRetur(String noRetur) {
        this.noRetur = noRetur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kdBrng != null ? kdBrng.hashCode() : 0);
        hash += (noRetur != null ? noRetur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KembaliPK)) {
            return false;
        }
        KembaliPK other = (KembaliPK) object;
        if ((this.kdBrng == null && other.kdBrng != null) || (this.kdBrng != null && !this.kdBrng.equals(other.kdBrng))) {
            return false;
        }
        if ((this.noRetur == null && other.noRetur != null) || (this.noRetur != null && !this.noRetur.equals(other.noRetur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.KembaliPK[kdBrng=" + kdBrng + ", noRetur=" + noRetur + "]";
    }

}
