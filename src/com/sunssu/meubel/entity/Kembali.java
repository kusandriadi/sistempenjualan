/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "kembali", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Kembali.findAll", query = "SELECT k FROM Kembali k"),
    @NamedQuery(name = "Kembali.findByKdBrng", query = "SELECT k FROM Kembali k WHERE k.kembaliPK.kdBrng = :kdBrng"),
    @NamedQuery(name = "Kembali.findByNoRetur", query = "SELECT k FROM Kembali k WHERE k.kembaliPK.noRetur = :noRetur"),
    @NamedQuery(name = "Kembali.findByJmlhBarang", query = "SELECT k FROM Kembali k WHERE k.jmlhBarang = :jmlhBarang"),
    @NamedQuery(name = "Kembali.findByKeterangan", query = "SELECT k FROM Kembali k WHERE k.keterangan = :keterangan")})
public class Kembali implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected KembaliPK kembaliPK;
    @Column(name = "jmlh_barang")
    private Integer jmlhBarang;
    @Column(name = "keterangan", length = 25)
    private String keterangan;
    @JoinColumn(name = "kd_brng", referencedColumnName = "kd_brng", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Barang barang;
    @JoinColumn(name = "no_retur", referencedColumnName = "no_retur", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Retur retur;

    public Kembali() {
    }

    public Kembali(KembaliPK kembaliPK) {
        this.kembaliPK = kembaliPK;
    }

    public Kembali(String kdBrng, String noRetur) {
        this.kembaliPK = new KembaliPK(kdBrng, noRetur);
    }

    public KembaliPK getKembaliPK() {
        return kembaliPK;
    }

    public void setKembaliPK(KembaliPK kembaliPK) {
        this.kembaliPK = kembaliPK;
    }

    public Integer getJmlhBarang() {
        return jmlhBarang;
    }

    public void setJmlhBarang(Integer jmlhBarang) {
        this.jmlhBarang = jmlhBarang;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    public Retur getRetur() {
        return retur;
    }

    public void setRetur(Retur retur) {
        this.retur = retur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kembaliPK != null ? kembaliPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kembali)) {
            return false;
        }
        Kembali other = (Kembali) object;
        if ((this.kembaliPK == null && other.kembaliPK != null) || (this.kembaliPK != null && !this.kembaliPK.equals(other.kembaliPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Kembali[kembaliPK=" + kembaliPK + "]";
    }

}
