/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "pesan", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Pesan.findAll", query = "SELECT p FROM Pesan p"),
    @NamedQuery(name = "Pesan.findByNoSp", query = "SELECT p FROM Pesan p WHERE p.pesanPK.noSp = :noSp"),
    @NamedQuery(name = "Pesan.findByKdBrng", query = "SELECT p FROM Pesan p WHERE p.pesanPK.kdBrng = :kdBrng"),
    @NamedQuery(name = "Pesan.findByJmlPesan", query = "SELECT p FROM Pesan p WHERE p.jmlPesan = :jmlPesan"),
    @NamedQuery(name = "Pesan.findByHargaPesan", query = "SELECT p FROM Pesan p WHERE p.hargaPesan = :hargaPesan"),
    @NamedQuery(name="Pesan.findByKdBrngAndNoSp", query="SELECT p FROM Pesan p WHERE p.pesanPK.kdBrng = :kdBrng AND p.pesanPK.noSp = :noSp")})
public class Pesan implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PesanPK pesanPK;
    @Column(name = "jml_pesan")
    private Integer jmlPesan;
    @Column(name = "harga_pesan")
    private Integer hargaPesan;
    @JoinColumn(name = "no_sp", referencedColumnName = "no_sp", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pesanan pesanan;
    @JoinColumn(name = "kd_brng", referencedColumnName = "kd_brng", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Barang barang;

    public Pesan() {
    }

    public Pesan(PesanPK pesanPK) {
        this.pesanPK = pesanPK;
    }

    public Pesan(int noSp, String kdBrng) {
        this.pesanPK = new PesanPK(noSp, kdBrng);
    }

    public PesanPK getPesanPK() {
        return pesanPK;
    }

    public void setPesanPK(PesanPK pesanPK) {
        this.pesanPK = pesanPK;
    }

    public Integer getJmlPesan() {
        return jmlPesan;
    }

    public void setJmlPesan(Integer jmlPesan) {
        this.jmlPesan = jmlPesan;
    }

    public Integer getHargaPesan() {
        return hargaPesan;
    }

    public void setHargaPesan(Integer hargaPesan) {
        this.hargaPesan = hargaPesan;
    }

    public Pesanan getPesanan() {
        return pesanan;
    }

    public void setPesanan(Pesanan pesanan) {
        this.pesanan = pesanan;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pesanPK != null ? pesanPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pesan)) {
            return false;
        }
        Pesan other = (Pesan) object;
        if ((this.pesanPK == null && other.pesanPK != null) || (this.pesanPK != null && !this.pesanPK.equals(other.pesanPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Pesan[pesanPK=" + pesanPK + "]";
    }

}
