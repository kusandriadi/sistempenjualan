/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "sj", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Sj.findAll", query = "SELECT s FROM Sj s"),
    @NamedQuery(name = "Sj.findByNoSj", query = "SELECT s FROM Sj s WHERE s.noSj = :noSj"),
    @NamedQuery(name = "Sj.findByTglSj", query = "SELECT s FROM Sj s WHERE s.tglSj = :tglSj"),
    @NamedQuery(name = "Sj.findByAlamat", query = "SELECT s FROM Sj s WHERE s.alamat = :alamat")})
public class Sj implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "no_sj", nullable = false, length = 5)
    private String noSj;
    @Column(name = "tgl_sj")
    @Temporal(TemporalType.DATE)
    private Date tglSj;
    @Column(name = "alamat", length = 25)
    private String alamat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sj")
    private List<Kirim> kirimList;
    @JoinColumn(name = "no_fak", referencedColumnName = "no_fak", nullable = false)
    @ManyToOne(optional = false)
    private Faktur noFak;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noSj")
    private List<Retur> returList;

    public Sj() {
    }

    public Sj(String noSj) {
        this.noSj = noSj;
    }

    public String getNoSj() {
        return noSj;
    }

    public void setNoSj(String noSj) {
        this.noSj = noSj;
    }

    public Date getTglSj() {
        return tglSj;
    }

    public void setTglSj(Date tglSj) {
        this.tglSj = tglSj;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<Kirim> getKirimList() {
        return kirimList;
    }

    public void setKirimList(List<Kirim> kirimList) {
        this.kirimList = kirimList;
    }

    public Faktur getNoFak() {
        return noFak;
    }

    public void setNoFak(Faktur noFak) {
        this.noFak = noFak;
    }

    public List<Retur> getReturList() {
        return returList;
    }

    public void setReturList(List<Retur> returList) {
        this.returList = returList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noSj != null ? noSj.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sj)) {
            return false;
        }
        Sj other = (Sj) object;
        if ((this.noSj == null && other.noSj != null) || (this.noSj != null && !this.noSj.equals(other.noSj))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Sj[noSj=" + noSj + "]";
    }

}
