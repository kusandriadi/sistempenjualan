/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "retur", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Retur.findAll", query = "SELECT r FROM Retur r"),
    @NamedQuery(name = "Retur.findByNoRetur", query = "SELECT r FROM Retur r WHERE r.noRetur = :noRetur"),
    @NamedQuery(name = "Retur.findByTglReturn", query = "SELECT r FROM Retur r WHERE r.tglReturn = :tglReturn")})
public class Retur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "no_retur", nullable = false, length = 5)
    private String noRetur;
    @Column(name = "tgl_return")
    @Temporal(TemporalType.DATE)
    private Date tglReturn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "retur")
    private List<Kembali> kembaliList;
    @JoinColumn(name = "no_sj", referencedColumnName = "no_sj", nullable = false)
    @ManyToOne(optional = false)
    private Sj noSj;

    public Retur() {
    }

    public Retur(String noRetur) {
        this.noRetur = noRetur;
    }

    public String getNoRetur() {
        return noRetur;
    }

    public void setNoRetur(String noRetur) {
        this.noRetur = noRetur;
    }

    public Date getTglReturn() {
        return tglReturn;
    }

    public void setTglReturn(Date tglReturn) {
        this.tglReturn = tglReturn;
    }

    public List<Kembali> getKembaliList() {
        return kembaliList;
    }

    public void setKembaliList(List<Kembali> kembaliList) {
        this.kembaliList = kembaliList;
    }

    public Sj getNoSj() {
        return noSj;
    }

    public void setNoSj(Sj noSj) {
        this.noSj = noSj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noRetur != null ? noRetur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retur)) {
            return false;
        }
        Retur other = (Retur) object;
        if ((this.noRetur == null && other.noRetur != null) || (this.noRetur != null && !this.noRetur.equals(other.noRetur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Retur[noRetur=" + noRetur + "]";
    }

}
