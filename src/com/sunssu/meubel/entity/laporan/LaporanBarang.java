/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity.laporan;

/**
 *
 * @author Kus Andriadi
 */
public class LaporanBarang {

    private String kd_brng;

    private String nm_brng;

    private String satuan;

    private int har_sat;

    private int stock;

    public int getHar_sat() {
        return har_sat;
    }

    public void setHar_sat(int har_sat) {
        this.har_sat = har_sat;
    }

    public String getKd_brng() {
        return kd_brng;
    }

    public void setKd_brng(String kd_brng) {
        this.kd_brng = kd_brng;
    }

    public String getNm_brng() {
        return nm_brng;
    }

    public void setNm_brng(String nm_brng) {
        this.nm_brng = nm_brng;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
