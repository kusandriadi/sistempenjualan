/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity.laporan;

/**
 *
 * @author Kus Andriadi
 */
public class LaporanPelanggan {

    private String kd_plg;

    private String nm_plg;

    private String almt_plg;

    private String tlp_plg;

    public String getAlmt_plg() {
        return almt_plg;
    }

    public void setAlmt_plg(String almt_plg) {
        this.almt_plg = almt_plg;
    }

    public String getKd_plg() {
        return kd_plg;
    }

    public void setKd_plg(String kd_plg) {
        this.kd_plg = kd_plg;
    }

    public String getNm_plg() {
        return nm_plg;
    }

    public void setNm_plg(String nm_plg) {
        this.nm_plg = nm_plg;
    }

    public String getTlp_plg() {
        return tlp_plg;
    }

    public void setTlp_plg(String tlp_plg) {
        this.tlp_plg = tlp_plg;
    }
}
