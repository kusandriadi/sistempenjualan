/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Kus Andriadi
 */
@Embeddable
public class KirimPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "no_sj", nullable = false, length = 5)
    private String noSj;
    @Basic(optional = false)
    @Column(name = "kd_brng", nullable = false, length = 5)
    private String kdBrng;

    public KirimPK() {
    }

    public KirimPK(String noSj, String kdBrng) {
        this.noSj = noSj;
        this.kdBrng = kdBrng;
    }

    public String getNoSj() {
        return noSj;
    }

    public void setNoSj(String noSj) {
        this.noSj = noSj;
    }

    public String getKdBrng() {
        return kdBrng;
    }

    public void setKdBrng(String kdBrng) {
        this.kdBrng = kdBrng;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noSj != null ? noSj.hashCode() : 0);
        hash += (kdBrng != null ? kdBrng.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KirimPK)) {
            return false;
        }
        KirimPK other = (KirimPK) object;
        if ((this.noSj == null && other.noSj != null) || (this.noSj != null && !this.noSj.equals(other.noSj))) {
            return false;
        }
        if ((this.kdBrng == null && other.kdBrng != null) || (this.kdBrng != null && !this.kdBrng.equals(other.kdBrng))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.KirimPK[noSj=" + noSj + ", kdBrng=" + kdBrng + "]";
    }

}
