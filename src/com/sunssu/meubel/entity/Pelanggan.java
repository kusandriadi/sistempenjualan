/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "pelanggan", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Pelanggan.findAll", query = "SELECT p FROM Pelanggan p"),
    @NamedQuery(name = "Pelanggan.findByKdPlg", query = "SELECT p FROM Pelanggan p WHERE p.kdPlg = :kdPlg"),
    @NamedQuery(name = "Pelanggan.findByNmPlg", query = "SELECT p FROM Pelanggan p WHERE p.nmPlg = :nmPlg"),
    @NamedQuery(name = "Pelanggan.findByAlmtPlg", query = "SELECT p FROM Pelanggan p WHERE p.almtPlg = :almtPlg"),
    @NamedQuery(name = "Pelanggan.findByTlpPlg", query = "SELECT p FROM Pelanggan p WHERE p.tlpPlg = :tlpPlg")})
public class Pelanggan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kd_plg", nullable = false, length = 5)
    private String kdPlg;
    @Column(name = "nm_plg", length = 25)
    private String nmPlg;
    @Column(name = "almt_plg", length = 50)
    private String almtPlg;
    @Column(name = "tlp_plg", length = 10)
    private String tlpPlg;
    @OneToMany(mappedBy = "kdPlg")
    private List<Pesanan> pesananList;

    public Pelanggan() {
    }

    public Pelanggan(String kdPlg) {
        this.kdPlg = kdPlg;
    }

    public String getKdPlg() {
        return kdPlg;
    }

    public void setKdPlg(String kdPlg) {
        this.kdPlg = kdPlg;
    }

    public String getNmPlg() {
        return nmPlg;
    }

    public void setNmPlg(String nmPlg) {
        this.nmPlg = nmPlg;
    }

    public String getAlmtPlg() {
        return almtPlg;
    }

    public void setAlmtPlg(String almtPlg) {
        this.almtPlg = almtPlg;
    }

    public String getTlpPlg() {
        return tlpPlg;
    }

    public void setTlpPlg(String tlpPlg) {
        this.tlpPlg = tlpPlg;
    }

    public List<Pesanan> getPesananList() {
        return pesananList;
    }

    public void setPesananList(List<Pesanan> pesananList) {
        this.pesananList = pesananList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kdPlg != null ? kdPlg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pelanggan)) {
            return false;
        }
        Pelanggan other = (Pelanggan) object;
        if ((this.kdPlg == null && other.kdPlg != null) || (this.kdPlg != null && !this.kdPlg.equals(other.kdPlg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Pelanggan[kdPlg=" + kdPlg + "]";
    }

}
