/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Kus Andriadi
 */
@Embeddable
public class PesanPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "no_sp", nullable = false)
    private int noSp;
    @Basic(optional = false)
    @Column(name = "kd_brng", nullable = false, length = 5)
    private String kdBrng;

    public PesanPK() {
    }

    public PesanPK(int noSp, String kdBrng) {
        this.noSp = noSp;
        this.kdBrng = kdBrng;
    }

    public int getNoSp() {
        return noSp;
    }

    public void setNoSp(int noSp) {
        this.noSp = noSp;
    }

    public String getKdBrng() {
        return kdBrng;
    }

    public void setKdBrng(String kdBrng) {
        this.kdBrng = kdBrng;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) noSp;
        hash += (kdBrng != null ? kdBrng.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PesanPK)) {
            return false;
        }
        PesanPK other = (PesanPK) object;
        if (this.noSp != other.noSp) {
            return false;
        }
        if ((this.kdBrng == null && other.kdBrng != null) || (this.kdBrng != null && !this.kdBrng.equals(other.kdBrng))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.PesanPK[noSp=" + noSp + ", kdBrng=" + kdBrng + "]";
    }

}
