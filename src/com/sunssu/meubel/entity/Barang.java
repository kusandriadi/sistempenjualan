/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Kus Andriadi
 */
@Entity
@Table(name = "barang", catalog = "sistempenjualan", schema = "")
@NamedQueries({
    @NamedQuery(name = "Barang.findAll", query = "SELECT b FROM Barang b"),
    @NamedQuery(name = "Barang.findByKdBrng", query = "SELECT b FROM Barang b WHERE b.kdBrng = :kdBrng"),
    @NamedQuery(name = "Barang.findByNmBrng", query = "SELECT b FROM Barang b WHERE b.nmBrng = :nmBrng"),
    @NamedQuery(name = "Barang.findBySatuan", query = "SELECT b FROM Barang b WHERE b.satuan = :satuan"),
    @NamedQuery(name = "Barang.findByHarSat", query = "SELECT b FROM Barang b WHERE b.harSat = :harSat"),
    @NamedQuery(name = "Barang.findByStock", query = "SELECT b FROM Barang b WHERE b.stock = :stock")})
public class Barang implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kd_brng", nullable = false, length = 5)
    private String kdBrng;
    @Column(name = "nm_brng", length = 20)
    private String nmBrng;
    @Column(name = "satuan", length = 10)
    private String satuan;
    @Column(name = "har_sat")
    private Integer harSat;
    @Column(name = "stock")
    private Integer stock;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barang")
    private List<Kembali> kembaliList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barang")
    private List<Pesan> pesanList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barang")
    private List<Kirim> kirimList;

    public Barang() {
    }

    public Barang(String kdBrng) {
        this.kdBrng = kdBrng;
    }

    public String getKdBrng() {
        return kdBrng;
    }

    public void setKdBrng(String kdBrng) {
        this.kdBrng = kdBrng;
    }

    public String getNmBrng() {
        return nmBrng;
    }

    public void setNmBrng(String nmBrng) {
        this.nmBrng = nmBrng;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public Integer getHarSat() {
        return harSat;
    }

    public void setHarSat(Integer harSat) {
        this.harSat = harSat;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<Kembali> getKembaliList() {
        return kembaliList;
    }

    public void setKembaliList(List<Kembali> kembaliList) {
        this.kembaliList = kembaliList;
    }

    public List<Pesan> getPesanList() {
        return pesanList;
    }

    public void setPesanList(List<Pesan> pesanList) {
        this.pesanList = pesanList;
    }

    public List<Kirim> getKirimList() {
        return kirimList;
    }

    public void setKirimList(List<Kirim> kirimList) {
        this.kirimList = kirimList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kdBrng != null ? kdBrng.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Barang)) {
            return false;
        }
        Barang other = (Barang) object;
        if ((this.kdBrng == null && other.kdBrng != null) || (this.kdBrng != null && !this.kdBrng.equals(other.kdBrng))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sunssu.meubel.entity.Barang[kdBrng=" + kdBrng + "]";
    }

}
