/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.table.model;

import com.sunssu.meubel.entity.Pelanggan;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kus Andriadi
 */
public class PelangganTableModel extends AbstractTableModel{

    private ArrayList<Pelanggan> list;

    public PelangganTableModel(ArrayList<Pelanggan> list){
        this.list = list;
    }

    public int getRowCount() {
        if(list == null){
            return 0;
        }else{
            return list.size();
        }
    }

    public int getColumnCount() {
        return 4;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return list.get(rowIndex).getKdPlg();
            case 1:
                return list.get(rowIndex).getNmPlg();
            case 2:
                return list.get(rowIndex).getAlmtPlg();
            case 3:
                return list.get(rowIndex).getTlpPlg();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Kode Pelanggan";
            case 1:
                return "Nama Pelanggan";
            case 2:
                return "Alamat";
            case 3:
                return "No. Telp";
            default:
                return null;
        }
    }

    public void addPelanggan(ArrayList<Pelanggan> pelanggan) {
        this.list = pelanggan;
        fireTableRowsInserted(0, getRowCount() - 1);
    }
}
