/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.table.model;

import com.sunssu.meubel.entity.Kirim;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kus Andriadi
 */
public class SuratJalanTableModel extends AbstractTableModel{

    private ArrayList<Kirim> list = new ArrayList<Kirim>();

    public SuratJalanTableModel(ArrayList<Kirim> list) {
        this.list = list;
    }

    public int getRowCount() {
        if(list == null){
            return 0;
        }else{
            return list.size();
        }
    }

    public int getColumnCount() {
        return 3;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return list.get(rowIndex).getKirimPK().getNoSj();
            case 1:
                return list.get(rowIndex).getKirimPK().getKdBrng();
            case 2:
                return list.get(rowIndex).getJmlKirim();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No. Surat Jalan";
            case 1:
                return "Kode Barang";
            case 2:
                return "Jumlah Kirim";
            default:
                return null;
        }
    }

    public void deleteTableSJ(int row){
        list.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void updateTableSJ(int row,Kirim kirim){
        list.set(row, kirim);
        fireTableRowsUpdated(row, row);
    }

    public void addTableSJ(Kirim kirim) {
        list.add(kirim);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
}
