/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.table.model;

import com.sunssu.meubel.entity.Pesan;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kus Andriadi
 */
public class FakturTableModel extends AbstractTableModel{

    private ArrayList<Pesan> list = new ArrayList<Pesan>();

    public FakturTableModel(ArrayList<Pesan> list) {
        this.list = list;
    }

    public FakturTableModel(){
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Kode Barang";
            case 1:
                return "Nama Barang";
            case 2:
                return "Satuan";
            case 3:
                return "Harga Barang";
            case 4:
                return "Jumlah Pesan";
            case 5:
                return "Jumlah Harga";
            default:
                return null;
        }
    }

    public int getRowCount() {
        if(list == null){
            return 0;
        }else{
            return list.size();
        }
    }

    public int getColumnCount() {
        return 6;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return list.get(rowIndex).getBarang().getKdBrng();
            case 1:
                return list.get(rowIndex).getBarang().getNmBrng();
            case 2:
                return list.get(rowIndex).getBarang().getSatuan();
            case 3:
                return  list.get(rowIndex).getBarang().getHarSat();
            case 4:
                return list.get(rowIndex).getJmlPesan();
            case 5:
                return list.get(rowIndex).getHargaPesan();
            default:
                return null;
        }
    }

    public void deleteTablePesan(int row){
        list.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void updateTablePesan(int row, Pesan pesan){
        list.set(row, pesan);
        fireTableRowsUpdated(row, row);
    }

    public void addTablePesan(Pesan pesan) {
        list.add(pesan);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }

}
