/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.table.model;

import com.sunssu.meubel.entity.Barang;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kus Andriadi
 */
public class BarangTableModel extends AbstractTableModel{

    private ArrayList<Barang> list;

    public BarangTableModel(ArrayList<Barang> list) {
        this.list = list;
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Kode Barang";
            case 1:
                return "Nama Barang";
            case 2:
                return "Satuan";
            case 3:
                return "Harga Barang";
            case 4:
                return "Stock";
            default:
                return null;
        }
    }

    public int getRowCount() {
        if(list == null){
            return 0;
        }else{
            return list.size();
        }
    }

    public int getColumnCount() {
        return  5;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return list.get(rowIndex).getKdBrng();
            case 1:
                return list.get(rowIndex).getNmBrng();
            case 2:
                return list.get(rowIndex).getSatuan();
            case 3:
                return list.get(rowIndex).getHarSat();
            case 4:
                return list.get(rowIndex).getStock();
            default:
                return null;
        }
    }

    public void addBarang(ArrayList<Barang> barang) {
        this.list = barang;
        fireTableRowsInserted(0, getRowCount() - 1);
    }

}
