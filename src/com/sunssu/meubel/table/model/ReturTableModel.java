/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.table.model;

import com.sunssu.meubel.entity.Kembali;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kus Andriadi
 */
public class ReturTableModel extends AbstractTableModel{

    private ArrayList<Kembali> list = new ArrayList<Kembali>();

    public ReturTableModel(ArrayList<Kembali> list) {
        this.list = list;
    }

    public int getRowCount() {
        if(list == null){
            return 0;
        }else{
            return list.size();
        }
    }

    public int getColumnCount() {
        return 3;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return list.get(rowIndex).getBarang().getKdBrng();
            case 1:
                return list.get(rowIndex).getJmlhBarang();
            case 2:
                return list.get(rowIndex).getKeterangan();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Kode Barang";
            case 1:
                return "Jumlah Retur";
            case 2:
                return "Keterangan";
            default:
                return null;
        }
    }

    public void deleteTableRetur(int row){
        list.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void updateTableRetur(int row,Kembali kembali){
        list.set(row, kembali);
        fireTableRowsUpdated(row, row);
    }

    public void addTableRetur(Kembali kembali) {
        list.add(kembali);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
}
