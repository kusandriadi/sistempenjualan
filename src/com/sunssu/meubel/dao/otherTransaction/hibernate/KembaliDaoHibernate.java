/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction.hibernate;

import com.sunssu.meubel.dao.otherTransaction.KembaliDao;
import com.sunssu.meubel.entity.Kembali;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("kembaliDaoHibernate")
public class KembaliDaoHibernate implements KembaliDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Kembali kembali) {
       sessionFactory.getCurrentSession().saveOrUpdate(kembali);
    }

    public void delete(Kembali kembali) {
        sessionFactory.getCurrentSession().delete(kembali);
    }

    public Kembali getKembaliByNoRetur(String kembali) {
       return (Kembali) sessionFactory.getCurrentSession().getNamedQuery("Kembali.findByNoRetur")
               .setParameter("noRetur", kembali);
    }

    public Kembali getKembaliByKdBarang(String kembali) {
        return (Kembali) sessionFactory.getCurrentSession().getNamedQuery("Kembali.findByKdBrng")
                .setParameter("kdBrng", kembali);
    }

    public List<Kembali> getKembali() {
        return sessionFactory.getCurrentSession().createQuery("from Kembali").list();
    }

}
