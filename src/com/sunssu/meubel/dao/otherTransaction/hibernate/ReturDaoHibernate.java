/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction.hibernate;

import com.sunssu.meubel.dao.otherTransaction.ReturDao;
import com.sunssu.meubel.entity.Retur;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("returDaoHibernate")
public class ReturDaoHibernate implements ReturDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Retur retur) {
        sessionFactory.getCurrentSession().saveOrUpdate(retur);
    }

    public void delete(Retur retur) {
       sessionFactory.getCurrentSession().saveOrUpdate(retur);
    }

    public Retur getRetur(String retur) {
       return (Retur) sessionFactory.getCurrentSession().getNamedQuery("Retur.findByNoRetur")
               .setParameter("noRetur", retur);
    }

    public List<Retur> getReturs() {
        return sessionFactory.getCurrentSession().createQuery("from Retur").list();
    }

    public String autoNumberRetur() {
        String text = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(noRetur) as Kode_Retur from Retur").list();
        for(Object result:o){
            text = (String) result;
        }
        if(text == null){
            return "";
        }else{
            return text;
        }
    }

}
