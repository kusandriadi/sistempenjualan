/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction.hibernate;

import com.sunssu.meubel.dao.otherTransaction.SuratJalanDao;
import com.sunssu.meubel.entity.Sj;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("suratJalanDaoHibernate")
public class SuratJalanDaoHibernate implements SuratJalanDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Sj sj) {
        System.out.println("onDao1");
        sessionFactory.getCurrentSession().saveOrUpdate(sj);
        System.out.println("onDao2");
    }

    public void delete(Sj sj) {
        sessionFactory.getCurrentSession().delete(sj);
    }

    public Sj getSuratJalan(String sj) {
        return (Sj) sessionFactory.getCurrentSession().getNamedQuery("Sj.findByNoSj")
                .setParameter("noSj", sj).uniqueResult();
    }

    public List<Sj> getSuratJalans() {
        return sessionFactory.getCurrentSession().createQuery("from Sj").list();
    }

    public String autoNumberSuratJalan() {
                String text = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(noSj) as Kode_SuratJalan from Sj").list();
        for(Object result:o){
            text = (String) result;
        }
        if(text == null){
            return "";
        }else{
            return text;
        }
    }

}
