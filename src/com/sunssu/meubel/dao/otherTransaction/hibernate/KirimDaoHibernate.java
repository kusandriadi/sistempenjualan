/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction.hibernate;

import com.sunssu.meubel.dao.otherTransaction.KirimDao;
import com.sunssu.meubel.entity.Kirim;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("kirimDaoHibernate")
public class KirimDaoHibernate implements KirimDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Kirim kirim) {
        sessionFactory.getCurrentSession().saveOrUpdate(kirim);
    }

    public void delete(Kirim kirim) {
        sessionFactory.getCurrentSession().delete(kirim);
    }

    public Kirim getKirimByNoSJ(String kirim) {
        return (Kirim) sessionFactory.getCurrentSession().getNamedQuery("Kirim.findByNoSj")
                .setParameter("noSj", kirim);
    }

    public Kirim getKirimByKdBarang(String kirim) {
       return (Kirim) sessionFactory.getCurrentSession().getNamedQuery("Kirim.findByKdBrng")
                .setParameter("kdBrng", kirim);
    }

    public List<Kirim> getKirim() {
       return sessionFactory.getCurrentSession().createQuery("from Kirim").list();
    }

}
