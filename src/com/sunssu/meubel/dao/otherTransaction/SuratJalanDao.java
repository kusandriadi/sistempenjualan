/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction;

import com.sunssu.meubel.entity.Sj;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface SuratJalanDao {
    
    public void saveOrUpdate(Sj sj);

    public void delete(Sj sj);

    public Sj getSuratJalan(String sj);

    public List<Sj> getSuratJalans();

    public String autoNumberSuratJalan();
}
