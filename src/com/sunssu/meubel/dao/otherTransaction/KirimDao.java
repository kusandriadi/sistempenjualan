/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction;

import com.sunssu.meubel.entity.Kirim;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface KirimDao {

    public void saveOrUpdate(Kirim kirim);

    public void delete(Kirim kirim);

    public Kirim getKirimByNoSJ(String kirim);

    public Kirim getKirimByKdBarang(String kirim);

    public List<Kirim> getKirim();

}
