/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction;

import com.sunssu.meubel.entity.Retur;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface ReturDao {

    public void saveOrUpdate(Retur retur);

    public void delete(Retur retur);

    public Retur getRetur(String retur);

    public List<Retur> getReturs();

    public String autoNumberRetur();

}
