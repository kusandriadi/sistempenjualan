/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.otherTransaction;

import com.sunssu.meubel.entity.Kembali;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface KembaliDao {

    public void saveOrUpdate(Kembali kembali);

    public void delete(Kembali kembali);

    public Kembali getKembaliByNoRetur(String kembali);

    public Kembali getKembaliByKdBarang(String kembali);

    public List<Kembali> getKembali();

}
