/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.master;

import com.sunssu.meubel.entity.Barang;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface BarangDao {

    public void saveOrUpdate(Barang barang);

    public void delete(Barang barang);

    public Barang getBarang(String barang);

    public List<Barang> getbBarangs();

    public String autoNumberBarang();
}
