/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.master;

import com.sunssu.meubel.entity.Pelanggan;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface PelangganDao {

    public void saveOrUpdate(Pelanggan pelanggan);

    public void delete(Pelanggan pelanggan);

    public Pelanggan getPelanggan(String pelanggan);

    public List<Pelanggan> getPelanggans();

    public String autoNumberPelanggan();

}
