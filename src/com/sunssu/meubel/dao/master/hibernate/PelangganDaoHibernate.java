/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.master.hibernate;

import com.sunssu.meubel.dao.master.PelangganDao;
import com.sunssu.meubel.entity.Pelanggan;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("pelangganDaoHibernate")
public class PelangganDaoHibernate implements PelangganDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Pelanggan pelanggan) {
        sessionFactory.getCurrentSession().saveOrUpdate(pelanggan);
    }

    public void delete(Pelanggan pelanggan) {
        sessionFactory.getCurrentSession().delete(pelanggan);
    }

    public Pelanggan getPelanggan(String pelanggan) {
        return (Pelanggan) sessionFactory.getCurrentSession()
                .getNamedQuery("Pelanggan.findByKdPlg")
                .setParameter("kdPlg", pelanggan).uniqueResult();
    }

    public List<Pelanggan> getPelanggans() {
        return sessionFactory.getCurrentSession().createQuery("from Pelanggan")
                .list();
    }

    public String autoNumberPelanggan() {
        String text = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(kdPlg) as Kode_Pelanggan from Pelanggan").list();
        for(Object result:o){
            text = (String) result;
        }
        return text;
    }
}
