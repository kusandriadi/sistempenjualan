/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.master.hibernate;

import com.sunssu.meubel.dao.master.BarangDao;
import com.sunssu.meubel.entity.Barang;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("barangDaoHibernate")
public class BarangDaoHibernate implements BarangDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Barang barang) {
        System.out.println("dao1");
        sessionFactory.getCurrentSession().saveOrUpdate(barang);
        System.out.println("dao2");
    }

    public void delete(Barang barang) {
        sessionFactory.getCurrentSession().delete(barang);
    }

    public Barang getBarang(String barang) {
        return (Barang) sessionFactory.getCurrentSession().getNamedQuery("Barang.findByKdBrng")
                .setParameter("kdBrng", barang).uniqueResult();
    }

    public List<Barang> getbBarangs() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Barang").list();
    }

    public String autoNumberBarang() {
        String text = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(kdBrng) as Kode_Barang from Barang").list();
        for(Object result:o){
            text = (String) result;
        }
        if(text == null){
            return "";
        }else{
            return text;
        }
    }
}
