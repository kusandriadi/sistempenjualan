/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction;

import com.sunssu.meubel.entity.Faktur;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface FakturDao {

    public void saveOrUpdate(Faktur faktur);

    public void delete(Faktur faktur);

    public Faktur getFaktur(String faktur);

    public List<Faktur> getFaktur();

    public String autoNumberFaktur();


}
