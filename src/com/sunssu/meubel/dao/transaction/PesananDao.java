/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction;

import com.sunssu.meubel.entity.Pesanan;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface PesananDao {

    public void saveOrUpdate(Pesanan pesanan);

    public void delete(Pesanan pesanan);

    public Pesanan getPesanan(int pesanan);

    public List<Pesanan> getPesanan();

    public String autoNumberPesanan();

}
