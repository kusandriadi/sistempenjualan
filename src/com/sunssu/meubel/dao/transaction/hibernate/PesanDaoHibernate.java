/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction.hibernate;

import com.sunssu.meubel.dao.transaction.PesanDao;
import com.sunssu.meubel.entity.Pesan;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("pesanDaoHibernate")
public class PesanDaoHibernate implements PesanDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Pesan pesan) {
        sessionFactory.getCurrentSession().saveOrUpdate(pesan);
    }

    public void delete(Pesan pesan) {
        sessionFactory.getCurrentSession().delete(pesan);
    }

    public Pesan getPesanByNoSP(String pesan) {
        return (Pesan) sessionFactory.getCurrentSession().getNamedQuery("Pesan.findByNoSp")
                .setParameter("pesanan", pesan).uniqueResult();
    }

    public Pesan getPesanByKdBarang(String pesan) {
        return (Pesan) sessionFactory.getCurrentSession().getNamedQuery("Pesan.findByKdBrng")
                .setParameter("kdBrng", pesan).uniqueResult();
    }

    public List<Pesan> getPesan() {
        return sessionFactory.getCurrentSession().createQuery("from Pesan").list();
    }

    public Pesan getForSuratJalan(int pesan, String pesanBarang) {
        return (Pesan) sessionFactory.getCurrentSession()
                .createQuery("from Pesan p where p.pesanPK.kdBrng =:kdBrng " +
                "and p.pesanPK.noSp = :noSp")
                .setParameter("kdBrng", pesanBarang)
                .setParameter("noSp", pesan).uniqueResult();
    }

    public List<String> getKodeBarangByNoSp(int pesan) {
        return sessionFactory.getCurrentSession()
                .createQuery("select p.pesanPK.kdBrng from Pesan p where p.pesanPK.noSp = :noSp")
                .setParameter("noSp", pesan).list();
    }

    public Pesan getDataBarangForSuratJalan(String barang, int pesan) {
        return (Pesan) sessionFactory.getCurrentSession()
                .getNamedQuery("Pesan.findByKdBrngAndNoSp")
                .setParameter("kdBrng", barang)
                .setParameter("noSp", pesan).uniqueResult();
    }
}
