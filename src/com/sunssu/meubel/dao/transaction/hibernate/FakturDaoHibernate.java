/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction.hibernate;

import com.sunssu.meubel.dao.transaction.FakturDao;
import com.sunssu.meubel.entity.Faktur;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("fakturDaoHibernate")
public class FakturDaoHibernate implements FakturDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Faktur faktur) {
        sessionFactory.getCurrentSession().saveOrUpdate(faktur);
    }

    public void delete(Faktur faktur) {
        sessionFactory.getCurrentSession().delete(faktur);
    }

    public Faktur getFaktur(String faktur) {
        return (Faktur) sessionFactory.getCurrentSession().getNamedQuery("Faktur.findByNoFak")
                .setParameter("noFak", faktur).uniqueResult();
    }

    public List<Faktur> getFaktur() {
        return sessionFactory.getCurrentSession().createQuery("from Faktur").list();
    }

    public String autoNumberFaktur() {
        String text = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(noFak) as Kode_Faktur from Faktur").list();
        for(Object result:o){
            text = (String) result;
        }
        if(text == null){
            return "";
        }else{
            return text;
        }
    }

}
