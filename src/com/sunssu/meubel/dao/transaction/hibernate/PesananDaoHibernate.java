/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction.hibernate;

import com.sunssu.meubel.dao.transaction.PesananDao;
import com.sunssu.meubel.entity.Pesanan;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kus Andriadi
 */
@Repository("pesananDaoHibernate")
public class PesananDaoHibernate implements PesananDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void saveOrUpdate(Pesanan pesanan) {
        sessionFactory.getCurrentSession().saveOrUpdate(pesanan);
    }

    public void delete(Pesanan pesanan) {
        sessionFactory.getCurrentSession().delete(pesanan);
    }

    public Pesanan getPesanan(int pesanan) {
         return (Pesanan) sessionFactory.getCurrentSession().getNamedQuery("Pesanan.findByNoSp")
                .setParameter("noSp", pesanan).uniqueResult();
    }

    public List<Pesanan> getPesanan() {
        return sessionFactory.getCurrentSession().createQuery("from Pesanan").list();
    }

    public String autoNumberPesanan() {
        String angka = null;
        List o;
        o = sessionFactory.getCurrentSession()
                .createQuery("select max(noSp) as Kode_Pesan from Pesanan").list();
        for(Object result:o){
            angka = String.valueOf(result);
        }
        if(angka == null){
            return "";
        }else{
            return angka;
        }
    }

}
