/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.dao.transaction;

import com.sunssu.meubel.entity.Pesan;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface PesanDao {

    public void saveOrUpdate(Pesan pesan);

    public void delete(Pesan pesan);

    public Pesan getPesanByNoSP(String pesan);

    public Pesan getPesanByKdBarang(String pesan);

    public Pesan getForSuratJalan(int pesan, String pesanBarang);

    public List<Pesan> getPesan();

    public List<String> getKodeBarangByNoSp(int pesan);

    public Pesan getDataBarangForSuratJalan(String barang, int pesan);

}
