/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.component;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Kus Andriadi
 */
public class PanelImage extends JPanel{

        private Image image;

    public PanelImage() {
        image = new ImageIcon(getClass().getResource("/com/sunssu/meubel/images/sunssuDialog.jpg")).getImage();
        setLayout(new BorderLayout());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D gd = (Graphics2D) g.create();

        gd.drawImage(image, 0, 0, null);

        gd.dispose();
    }

}
