/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.component;

import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Kus Andriadi
 */
public class LimitTextField extends PlainDocument{

    private int limit;

	public LimitTextField(int limit){
		super();
		this.limit = limit;
	}

	@Override
	public void insertString(int offs, String str, AttributeSet a){
		if(str==null){
			return;
		}

		if((getLength() + str.length()) <= limit){
			try {
				super.insertString(offs, str, a);
			} catch (BadLocationException e) {
				JOptionPane.showMessageDialog(null, "ERROR : " + e.getMessage());
			}
		}
	}

}
