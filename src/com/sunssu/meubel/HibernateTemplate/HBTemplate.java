/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sunssu.meubel.HibernateTemplate;

import com.sunssu.meubel.service.master.MasterService;
import com.sunssu.meubel.service.transaction.PesanService;
import com.sunssu.meubel.service.transaction.SendService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Kus Andriadi
 */
public class HBTemplate {

    private static ApplicationContext applicationContext;
    
    public static ApplicationContext getApplicationContext() {
        applicationContext = new FileSystemXmlApplicationContext("/conf/spring-ctx.xml");
        return applicationContext;
    }

    public static MasterService getMasterService(){
        return (MasterService) applicationContext.getBean("masterService");
    }

    public static PesanService getPesanService(){
        return (PesanService) applicationContext.getBean("pesanService");
    }

    public static SendService getSendService(){
        return (SendService) applicationContext.getBean("sendService");
    }

}
