﻿drop database if exists SistemPenjualan;

create database SistemPenjualan;

use SistemPenjualan;

create table pelanggan(
	kd_plg varchar(5) primary key not null,
	nm_plg varchar(25),
	almt_plg varchar(50),
	tlp_plg varchar(10)
);

create table pesanan(
	no_sp int auto_increment primary key not null,
	tgl_sp date,
	kd_plg varchar(5),
	total int(10),
	FOREIGN KEY(kd_plg)
	REFERENCES pelanggan(kd_plg)
	ON DELETE CASCADE ON UPDATE CASCADE
);

create table barang(
	kd_brng varchar(5) primary key not null,
	nm_brng varchar(20),
	satuan varchar(10),
	har_sat int(11),
	stock int(2)
);

create table pesan(
	no_sp int not null,
	kd_brng varchar(5) not null,
	jml_pesan int(4),
	harga_pesan int(11),
	primary key (no_sp, kd_brng),
	foreign key(no_sp) references pesanan(no_sp) on delete cascade on update cascade,
	foreign key(kd_brng) references barang(kd_brng) on delete cascade on update cascade
);

create table faktur(
	no_fak varchar(5) not null primary key,
	no_sp int not null,
	tgl_fak date,
	foreign key(no_sp) references pesanan(no_sp) on delete cascade on update cascade
);

create table sj(
	no_sj varchar(5) primary key not null,
	no_fak varchar(5) not null,
	tgl_sj date,
	alamat varchar(25),
	foreign key(no_fak) references faktur(no_fak) on delete cascade on update cascade
);

create table kirim(
	no_sj varchar(5) not null,
	kd_brng varchar(5) not null,
	jml_kirim int,
	primary key(no_sj, kd_brng),
	foreign key(no_sj) references sj(no_sj) on delete cascade on update cascade,
	foreign key(kd_brng) references barang(kd_brng) on delete cascade on update cascade
);

create table retur(
	no_retur varchar(5) not null primary key,
	tgl_return date,
	no_sj varchar(5) not null,
	foreign key(no_sj) references sj(no_sj) on delete cascade on update cascade
);

create table kembali(
	kd_brng varchar(5) not null,
	no_retur varchar(5) not null,
	jmlh_barang int,
	keterangan varchar(25),
	primary key(kd_brng, no_retur),
	foreign key(kd_brng) references barang(kd_brng) on delete cascade on update cascade,
	foreign key(no_retur) references retur(no_retur) on delete cascade on update cascade
);

insert into pelanggan values('P0001','Peter','Jl. Mangga','0217829173');
insert into pelanggan values('P0002','Dodo Haryana','Jl. Pisang','0218392182');
insert into pelanggan values('P0003','Septyasari Putri','Jl. Duren','0212918391');
insert into pelanggan values('P0004','Dona Hernando','Jl. Inpres','0212910293');
insert into pelanggan values('P0005','Mio Duljana','Jl. Deplu','0212938191');
insert into pelanggan values('P0006','Bambang','Jl. Mangga Busuk','0211145453');
insert into pelanggan values('P0007','Sugeng','Jl. ASPOL','0212455359');
insert into pelanggan values('P0008','Ningsih Sunarya','Jl. Dirman','0215645589');
insert into pelanggan values('P0009','Asep Sunandar','Jl. Duku','0214686265');
insert into pelanggan values('P0010','Budi','Jl. Anggur','0216469354');
insert into pelanggan values('P0011','Susi Susanti','Jl. Durian','0214639877');
insert into pelanggan values('P0012','Hernanto','Jl. Belakang','0215436976');
insert into pelanggan values('P0013','Hendro','Jl. Caplin','0219875642');
insert into pelanggan values('P0014','Dodi','Jl. Pahlawan','0211335486');
insert into pelanggan values('P0015','Afran','Jl. Becak','0211263548');
insert into pelanggan values('P0016','Dodol','Jl. Musang','0214568721');
insert into pelanggan values('P0017','Sri Mulyati','Jl. Kucing','0214568743');
insert into pelanggan values('P0018','Ayu Sari','Jl. Speaker','0214575987');
insert into pelanggan values('P0019','Wulan Septyasari','Jl. Komputer','0214231898');
insert into pelanggan values('P0020','Putri Kumalawati','Jl. CPU','0214668754');
insert into pelanggan values('P0021','Hernando','Jl. Depan','0214587321');
insert into pelanggan values('P0022','Brotosaputro','Jl. Kelurahan','0217893248');
insert into pelanggan values('P0023','Sukemi','Jl. Orang','0217892135');
insert into pelanggan values('P0024','Jaja Miharja','Jl. Keyboard','0214246874');
insert into pelanggan values('P0025','Sudimampir','Jl. Inpres II','0213654684');
insert into pelanggan values('P0026','Helios','Jl. Mangga II','0211235411');
insert into pelanggan values('P0027','Mark Van Boihell','Jl. Mangga III','0211385421');
insert into pelanggan values('P0028','Roni','Jl. Jeruk','0216513210');
insert into pelanggan values('P0029','Ryo Fernando','Jl. Mangga Jeruk','0217831213');
insert into pelanggan values('P0030','Melati','Jl. Rubik','0214562132');

insert into barang values('B0001','Meja Komputer','Buah','100000','9');
insert into barang values('B0002','Meja Belajar','Buah','100000','10');
insert into barang values('B0003','Meja Lipat','Buah','150000','4');
insert into barang values('B0004','Kursi Goyang','Buah','200000','3');
insert into barang values('B0005','Kursi Antik','Buah','700000','5');
insert into barang values('B0006','Lemari Anak','Buah','70000','9');
insert into barang values('B0007','Lemari 2 Pintu','Buah','200000','6');
insert into barang values('B0008','Lemari Sedang','Buah','150000','4');
insert into barang values('B0009','Lemari Sepatu','Buah','300000','3');
insert into barang values('B0010','Kursi Lipat','Buah','100000','2');
insert into barang values('B0011','Jendela','Buah','350000','1');
insert into barang values('B0012','Meja makan','Buah','700000','6');
insert into barang values('B0013','Kursi Anak','Buah','40000','6');
insert into barang values('B0014','Kursi Motif','Buah','500000','2');
insert into barang values('B0015','Lemari Besar','Buah','900000','5');
insert into barang values('B0016','Pintu','Buah','175000','7');
insert into barang values('B0017','Meja Komputer II','Buah','230000','5');
insert into barang values('B0018','Meja Kantor','Buah','250000','3');
insert into barang values('B0019','Meja Kantor Sedang','Buah','220000','6');
insert into barang values('B0020','Meja Kantor Besar','Buah','400000','2');
insert into barang values('B0021','Laci','Buah','75000','7');